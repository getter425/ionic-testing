var gulp = require('gulp');



var Server = require('karma').Server;

gulp.task('test', function(done) {
    var config = {
        configFile: __dirname + '/tests/my.conf.js',
        singleRun: true,
        autoWatch: false
    };

    var server = new Server(config, done);
    server.start();
});
